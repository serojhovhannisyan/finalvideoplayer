package com.team.hrayr.mediaplayer;

/**
 * Created by Hrayr on 20.01.2018.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {
    private ArrayList<Video> videoList;
    private Context mContext;

    protected VideoAdapter(ArrayList<Video> videoList, Context mContext) {
        this.videoList = videoList;
        this.mContext = mContext;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        ArrayList<Video> mvideoList = new ArrayList<>();
        Context mContext;

        protected MyViewHolder(View view, Context mContext, ArrayList<Video> mvideoList) {
            super(view);
            this.mvideoList = mvideoList;
            this.mContext = mContext;
            view.setOnClickListener(this);
            title = view.findViewById(R.id.title);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Video video = this.mvideoList.get(position);
            Intent intent = new Intent(mContext, MediaPlayerActivity.class);
            intent.putExtra("video", video.getVideo());
            this.mContext.startActivity(intent);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_row, parent, false);
        return new MyViewHolder(itemView, mContext, videoList);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Video video = videoList.get(position);
        holder.title.setText(video.getTitle());
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }
}

