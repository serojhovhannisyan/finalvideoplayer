package com.team.hrayr.mediaplayer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
//Changed  project

public class Launcher extends AppCompatActivity {
    private ArrayList<Video> videoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private VideoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        videoData();
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new VideoAdapter(videoList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void videoData() {
        Video video = new Video("Video1", R.raw.video1);
        videoList.add(video);
//
//        video = new Video("Video2", R.raw.video2);
//        videoList.add(video);
//
//        video = new Video("Video3", R.raw.video3);
//        videoList.add(video);
//
//        video = new Video("Video4", R.raw.video4);
//        videoList.add(video);
    }
}

