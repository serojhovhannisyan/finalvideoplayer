package com.team.hrayr.mediaplayer;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class MediaPlayerActivity extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, View.OnClickListener {
    private MediaPlayer mediaPlayer;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private ImageButton playPause, share;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private Runnable runnable;
    private TextView videoPosition, videoDuration;
    private int restoredPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);
        surfaceView = findViewById(R.id.surface_view);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mediaPlayer = MediaPlayer.create(this, R.raw.video1);
        initViews();
        initListeners();
        startMediaPlayer();
        initMediaplayerTime();
    }

    private void initViews() {
        videoPosition = findViewById(R.id.time_position);
        videoDuration = findViewById(R.id.time_duration);
        seekBar = findViewById(R.id.seekbar);
        share = findViewById(R.id.share);
        playPause = findViewById(R.id.play_pause);
        playPause.setOnClickListener(this);
        playPause.setImageResource(R.drawable.pause);
        playPause.setBackgroundColor(Color.BLACK);
    }


    private void initListeners() {
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Share is clicked", Toast.LENGTH_SHORT).show();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                if (b) {
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }

    private void startMediaPlayer() {
        seekBar.setProgress(mediaPlayer.getCurrentPosition());
        if (mediaPlayer.isPlaying()) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    startMediaPlayer();
                }
            };
            handler.postDelayed(runnable, 1000);
        }
    }

    private void initMediaplayerTime() {
        int current_time = mediaPlayer.getCurrentPosition();
        int video_duration = mediaPlayer.getDuration();
        videoDuration.setText(String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes((long) video_duration),
                TimeUnit.MILLISECONDS.toSeconds((long) video_duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                video_duration)))
        );

        videoPosition.setText(String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes((long) current_time),
                TimeUnit.MILLISECONDS.toSeconds((long) current_time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                current_time))));
        seekBar.setProgress(current_time);
        handler.postDelayed(UpdateVideoTime, 100);
    }

    private Runnable UpdateVideoTime = new Runnable() {

        public void run() {

            if (mediaPlayer != null) {
                int current_time = mediaPlayer.getCurrentPosition();
                videoPosition.setText(String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes((long) current_time),
                        TimeUnit.MILLISECONDS.toSeconds((long) current_time) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes((long) current_time))));
                seekBar.setProgress(current_time);
            }
            handler.postDelayed(this, 100);
        }
    };

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDisplay(surfaceHolder);
            String path = "android.resource://" + getPackageName() + "/" + R.raw.video1;
            mediaPlayer.setDataSource(this, Uri.parse(path));
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    boolean first_time = true;// checking for lifecycles
    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.e("Prepared", "onPrepared called");
        seekBar.setMax(mediaPlayer.getDuration());
        mediaPlayer.seekTo(restoredPosition);
        if(first_time)
        {
            Log.e("onPrepared", "onPrepared called");
            mediaPlayer.start();
        }
    }

    private void stopThread() {
        handler.removeCallbacks(UpdateVideoTime);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new Thread(new Runnable() {
            @Override
            public void run() {
                stopThread();
            }
        }).start();
    }


    @Override
    protected void onPause() {
        Log.e("onPause", "onPause called");
        super.onPause();
        restoredPosition = mediaPlayer.getCurrentPosition();
        first_time = false; //for second and + time calling onPrepared()
        isPlaying = true;
        playPause.setImageResource(R.drawable.play);
        releaseMediaPlayer();

    }

    @Override
    protected void onResume() {  //?
        Log.e("onResume", "onResume called");
        super.onResume();
        if(!first_time )
        {
            playPause.setImageResource(R.drawable.play);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        isPlaying = true;  // for repeating video
        if(mediaPlayer != null )
        {
            playPause.setImageResource(R.drawable.play);
        }
    }

    boolean isPlaying = false;
    @Override
    public void onClick(View view) {
            if (!isPlaying ) {
                playPause.setImageResource(R.drawable.play);
                mediaPlayer.pause();
                isPlaying = true;
            }
            else {
                playPause.setImageResource(R.drawable.pause);
                mediaPlayer.start();
                isPlaying = false;
            }
        }

    private void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}