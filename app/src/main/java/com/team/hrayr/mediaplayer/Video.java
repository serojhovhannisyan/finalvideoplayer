package com.team.hrayr.mediaplayer;

/**
 * Created by Hrayr on 20.01.2018.
 */

public class Video {
    private String title;
    private int video;

    public Video(String title, int video) {
        this.title = title;
        this.video = video;
    }

    public String getTitle() {
        return title;
    }

    protected int getVideo() {
        return video;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVideo(int video) {
        this.video = video;
    }
}
